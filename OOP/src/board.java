
public class board {
	private player X;
	private player O;
	private player currentPlayer;
	private int turnCount;
	private player winner;
	private char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };

	public board(player X, player O) {
		this.X = X;
		this.O = O;
		currentPlayer = X;
	}

	public char[][] getTable() {
		return table;

	}

	public player getCurrentPlayer() {
		return currentPlayer;
	}

	public boolean setTable(int row, int col) {
		if (table[row][col] == '-') {
			table[row][col] = currentPlayer.getName();
			return true;
		}
		return false;
	}

	private boolean checkRow(int row) {
		for (int col = 0; col < table[row].length; col++) {
			if (table[row][col] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}

	private boolean checkCol(int col) {
		for (int row = 0; row < table.length; row++) {
			if (table[row][col] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}

	private boolean checkRow() {
		if (checkRow(0) || checkRow(1) || checkRow(2)) {
			return true;
		}
		return false;
	}

	private boolean checkCol() {
		if (checkCol(0) || checkCol(1) || checkCol(2)) {
			return true;
		}
		return false;
	}

	private boolean checkX1() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][i] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}

	private boolean checkX2() {
		for (int i = 0; i < table.length; i++) {
			if (table[2 - i][i] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}

	private boolean checkDraw() {
		if (turnCount == 8) {
			X.draw();
			O.draw();
			return true;
		}
		return false;

	}

	public boolean checkWin() {
		if (checkRow() || checkCol() || checkX1() || checkX2()) {
			winner = currentPlayer;
			if (currentPlayer == X) {
				X.win();
				O.lose();
			} else {
				O.win();
				X.lose();
			}
			return true;
		}
		return false;
	}

	public boolean isFinish() {
		if (checkWin()) {
			return true;
		}
		if (checkDraw()) {

			return true;
		}

		return false;
	}

	public void switchPlayer() {
		if (currentPlayer == X) {
			currentPlayer = O;

		} else {
			currentPlayer = X;
		}
		turnCount++;
	}
	public player getWinner() {
		return winner;
	}
}
