
public class player {
	private char name;
	private int win, draw, lose;

	public player(char name) {
		this.name = name;
	}

	public char getName() {
		return name;
	}

	public void win() {
		win++;

	}

	public void draw() {
		draw++;
	}

	public void lose() {
		lose++;
	}

	 int getWin() {
		return win;
	}

	public int getDraw() {
		return draw;
	}

	public int getLose() {
		return lose;
	}
}
